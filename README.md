# SurveyAMC

The SurveyAMC project is targeted to create machine readable questionnaires for self-administered paper surveys in high typesetting quality. SurveyAMC is available as a package option of the Auto-Multiple-Choice Software. The included LaTeX package provides a style for designing the questionnaire’s general layout, as well as for formatting and placing the questions, the answers, the answer boxes, the completion and filter instructions. After the completion of the survey, the respondents’ answers can be automatically processed into a fully structured file (CSV). 

## Installation

To create a machine readable questionnaire in high typographical quality, you will need a LaTeX distribution, a LaTeX editor, a PDF viewer and the Auto-Multiple-Choice Software, or alternatively the implemented LaTeX package:

\usepackage[survey]{automultiplechoice}

Both, you can download from the Auto-Multiple-Choice GitLab repository: 

https://gitlab.com/jojo_boulix/auto-multiple-choice

Further information on installation and usage, you will find in the SurveyAMC Manual.

## Tutorial

The short Tutorial guides through the first steps of programming a machine readable questionnaire with LaTeX. Using the practical example of Marx' Workers' Inquiry, the Tutorial explains how to program single-choice, multiple-choice, matrix, and open-ended questions. Additionally, the tutorial gives a short introduction to adding supportive graphical elements like filter instructions or colored background boxes.

## Manual

The Manual explains in detail how you can apply graphical design principles in your paper-and-pencil questionnaire. For single-choice, multiple-choice, and matrix questions, various examples show how to format value labels, rotate answer boxes, implement navigational arrows, add open-answer fields, or connect answer boxes with lines. Furthermore, the Manual shows how you can use metadata for more efficient questionnaire programming. 


## Templates

The SurveyAMC project provides various LaTeX templates. These can help to get a first impression of how to program different questionnaire layouts. Among the questionnaires, you will find the Potsdam and the Konstanz example from the gallery. The templates are updated continuously. Feel free to download the templates and modify them. 


