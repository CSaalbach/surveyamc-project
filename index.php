
<!DOCTYPE html>
<html lang="de">
<head>
	<meta charset="utf-8"/>
	<meta content="IE=edge" http-equiv="X-UA-Compatible"/>
	<meta content="width=device-width, initial-scale=1" name="viewport"/>

	<title>SurveyAMC</title>
	
	<link href="../favicon.ico" rel="shortcut icon" type="image/x-icon"/>
	
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	
	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

	<link href="../styles.css" rel="stylesheet"/>
	  
	<link href="https://fonts.googleapis.com/css?family=Roboto+Slab:400,500|Roboto:400,500&display=swap" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css2?family=Open+Sans:ital,wght@0,300;0,400;0,600;0,700;0,800;1,300;1,400&display=swap" rel="stylesheet"> 
	<link rel="preconnect" href="https://fonts.gstatic.com">
	<link href="https://fonts.googleapis.com/css2?family=Cutive+Mono&display=swap" rel="stylesheet"> 
</head>

<body>
	<header>
		<nav class="navbar navbar-expand-md navbar-light bg-light">
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo03" aria-controls="navbarTogglerDemo03" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
			</button>
		  
		  	<a class="navbar-brand" href="https://survey.codes/">survey.codes</a>

		  	<div class="collapse navbar-collapse" id="navbarTogglerDemo03">

				<div class="container">
					
			    	<ul class="navbar-nav mr-auto mt-2 mt-lg-0 justify-content-center">
			      		<li class="nav-item active">
			        		<a class="nav-link" href="../surveyamc">SurveyAMC</a>
			      		</li>

			      		<li class="nav-item">
			        		<a class="nav-link" href="../click">ClickAMC</a>
			      		</li>

			      		<li class="nav-item">
			        		<a class="nav-link" href="../datatblr">Datatblr</a>
			      		</li>

						<li class="nav-item">
			        		<a class="nav-link" href="../mendel">Mendel</a>
			      		</li>		      		
			    	</ul>		    
				</div>
			</div>
			<img class="d-none d-sm-none d-md-block" src="../img/samc-logo7.svg" alt="surveyamc-logo" style="width:8%; margin:20px">
		</nav>
	</header>

	<div class="container">  				  	
	  	<div id="column">
		    <h2 class="mt-3" style="text-align: center">SurveyAMC</h2>
		    <p>
		    	The SurveyAMC project is targeted to create machine readable questionnaires for self-administered paper surveys in high typesetting quality. SurveyAMC is available as a package option of the <a style="color: #2C3744;font-weight: 550;" href="https://www.auto-multiple-choice.net/">Auto-Multiple-Choice Software</a>. The included LaTeX package provides a style for designing the questionnaire’s general layout, as well as for formatting and placing the questions, the answers, the answer boxes, the completion and filter instructions. After the completion of the survey, the respondents’ answers can be automatically processed into a fully structured file (CSV). 
		    </p>
		</div>
	</div>	


	<div class="container">  				  	
	  	<div id="column">
		    <h2 class="mt-3" style="text-align: center;">Gallery</h2>
			<p>
				The gallery presents some sample questionnaire layouts that are programmed SurveyAMC. The examples are newly designed versions of the Public Opinion Survey of the City of Potsdam (2018) and the City of Konstanz (2017).
			</p>	
			<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
			  	<ol class="carousel-indicators">
			    	<li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>

			    	<li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
			    	
			    	<li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
			  	</ol>
			 
			  	<div class="carousel-inner" style=" background-color: #f8f9fa !important; border: 1px solid #F3C71E;">
			    
			    	<div class="carousel-item active">
				      	<a href="../pdf/potsdam.pdf"><img src="../img/quest-po-1.svg" class="d-block w-50" alt="questionnaire example 1"></a>
				    </div>

				    <div class="carousel-item">
				    	<a href="../pdf/potsdam.pdf"><img src="../img/quest-po-2.svg" class="d-block w-50" alt="questionnaire example 2"></a>
				    </div>

				    <div class="carousel-item">
				    	<a href="../pdf/potsdam.pdf"><img src="../img/quest-po-3.svg" class="d-block w-50" alt="questionnaire example 3"></a>
				    </div>			    
			    
				    <div class="carousel-item">
				    	<a href="../pdf/konstanz.pdf"><img src="../img/quest-kn-1.svg" class="d-block w-50" alt="questionnaire example 4"></a>
				    </div>

				    <div class="carousel-item">
				    	<a href="../pdf/konstanz.pdf"><img src="../img/quest-kn-2.svg" class="d-block w-50" alt="questionnaire example 5"></a>
				    </div>

				    <div class="carousel-item">
				    	<a href="../pdf/konstanz.pdf"><img src="../img/quest-kn-3.svg" class="d-block w-50" alt="questionnaire example 6"></a>
				    </div>
			  	</div>
			 
				<a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
				    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
				    <span class="sr-only">Previous</span>
				</a>
			  
			  	<a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
			    	<span class="carousel-control-next-icon" aria-hidden="true"></span>
			    	<span class="sr-only">Next</span>
			  	</a>
			</div>    

<br><br>


		    <h2 class="mt-5" style="text-align: center">Getting Started</h2>
<br><br>

			<div class="row">

				<div class="card-deck">
					<div class="card">
				    
				    	<div class="card-body">
					      	<h5 class="card-title"><img src="../img/logo-installation.svg" alt="Logo Installation" style="width:40px"> Installation</h5>
					      	<p class="card-text">
					      		<p>To create a machine readable questionnaire in high typographical quality, you will need a LaTeX distribution, a LaTeX editor, a PDF viewer and the Auto-Multiple-Choice Software, or alternatively the implemented LaTeX package: </p>

					      		<p style="font-family: 'Cutive Mono', monospace; font-size: small">\usepackage[survey]{automultiplechoice}</span> </p>

					      		<p>Both, you can download from the Auto-Multiple-Choice</p>
 								
 								<p>
					      			<a href="https://gitlab.com/jojo_boulix/auto-multiple-choice" target="_blank" class="btn btn-primary">GitLab repository</a>
					      		</p>	

					      		<p>Further information on installation and usage, you will find in the SurveyAMC Manual.</p>
					      	</p>
					    </div>  	
				    </div>
								
					<div class="card">
				       	<div class="card-body">
					      	<h5 class="card-title"><img src="../img/logo-tutorial.svg" alt="Logo Tutorial" style="width:40px">Tutorial</h5>
					      	<p class="card-text">
					      		The short Tutorial guides through the first steps of programming a machine readable questionnaire with LaTeX. Using the practical example of Marx' Workers' Inquiry, the Tutorial explains how to program single-choice, multiple-choice, matrix, and open-ended questions. Additionally, the tutorial gives a short introduction to adding supportive graphical elements like filter instructions or colored background boxes.
					      	</p>

							<p>
   						      	<a href="../pdf/tutorial.pdf" target="_blank" class="btn btn-primary">view</a>
   						    </p>					      	
				    	</div>
					</div>
				</div>
			</div>	
<br>
			<div class="row">

				<div class="card-deck">

					<div class="card">
				       	<div class="card-body">
					      	<h5 class="card-title"><img src="../img/logo-book.svg" alt="Logo Manual" style="width:35px">Manual</h5>
					      	<p class="card-text">
					      		The Manual explains in detail how you can apply graphical design principles in your paper-and-pencil questionnaire. For single-choice, multiple-choice, and matrix questions, various examples show how to format value labels, rotate answer boxes, implement navigational arrows, add open-answer fields, or connect answer boxes with lines. Furthermore, the Manual shows how you can use metadata for more efficient questionnaire programming. 
					      	</p>

							<p>
   						      	<a href="https://survey.codes/pdf/surveyamc_manual.pdf" target="_blank" class="btn btn-primary">view</a>
   						    </p>		

				    	</div>
					</div>					


					<div class="card">
				       	<div class="card-body">
					      	<h5 class="card-title"><img src="../img/logo-templates.svg" alt="Logo Templates" style="width:30px;">Templates</h5>
					      	<p class="card-text">
					      		The SurveyAMC project provides various LaTeX templates. These can help to get a first impression of how to program different questionnaire layouts. Among the questionnaires, you will find the Potsdam and the Konstanz example from the gallery. The templates are updated continuously. Feel free to download the templates and modify them.  
					      	</p>
					      	<br><br>

							<p>
   						      	<a href="https://gitlab.com/CSaalbach/surveyamc-project/-/tree/master/templates" target="_blank" class="btn btn-primary">view</a>
   						    </p>					      	


				    	</div>
					</div>						

				</div><!--deck-->		

			</div><!--row-->

		</div><!--column-->

	</div><!--container-->


	<div class="card text-center" style="margin-top: 50px;">
  		
  		<div class="card-header">    
  		</div>

  		<div class="card-body">
    		<h5 class="card-title">Contact</h5>
    		<p style="text-align: center;">
    			<a href="https://www.uni-potsdam.de/soziologie-methoden/saalbach.php" target="_blank" style="color: #484F56 !important;">Claudia Saalbach</a>, <a href="https://www.uni-potsdam.de" target="_blank" style="color: #484F56 !important;">University of Potsdam</a>	
    		</p>
    		
  		</div>
  
  		<div class="card-footer text-muted">
    		<a href="https://www.uni-potsdam.de/soziologie-methoden/impressum.php" target="_blank" style="color: #484F56 !important;">Impressum</a>
  		</div>
	</div>





    <!-- jQuery (Bootstrap JS plugins depend on it) -->
    <script src="../jsn/jquery-3.1.0.min.js"></script>
    <script src="../jsn/bootstrap.min.js"></script>
    <script src="../jsn/script.js"></script>
 </body>
</html>






         





